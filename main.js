const { app, BrowserWindow, screen, ipcMain, Menu} = require('electron')
const path = require("path")
var mainWindow;
function createWindow () {
    app.userAgentFallback = app.userAgentFallback.replace('Electron/' + process.versions.electron, '');
    var mainScreen = screen.getPrimaryDisplay();
    var dimensions = mainScreen.size;
    var menu = Menu.buildFromTemplate([])
    Menu.setApplicationMenu(menu);
    var mainWindow = new BrowserWindow({
        width: dimensions.width,
        height: dimensions.height,
        webPreferences: {
            nodeIntegration: true,
            webviewTag: true,
            enableBlinkFeatures: false
        }
    })

    mainWindow.loadFile('index.html', {userAgent: 'Chrome'})
}

app.whenReady().then(createWindow)
app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', () => {
    if (mainWindow === null) {
        createWindow()
    }
})
ipcMain.on('quit', (event, arg) => {
    app.quit()
})